//package com.bby.test;
//
//import static org.hamcrest.core.Is.is;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//
//import java.util.LinkedList;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import org.springframework.http.MediaType;
//
//
//import com.bby.authorization.BigQueryAuthorization;
//import com.bby.main.BigQueryApp;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes=BigQueryApp.class)
//public class SampleTest {
//@Autowired
//private WebApplicationContext webApplicationContext;
//private MockMvc mockMvc;
//@Autowired
//BigQueryAuthorization auth;
//
//@Before
//  public void setup() {
//	mockMvc=MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//
//}
//
//@Test
//public void getResults() throws Exception{
//	mockMvc.perform(get("/showResults")).andExpect(status().isOk())
//	.andExpect(content().contentType("application/json;charset=UTF-8"))
//	.andExpect(jsonPath("$[0].id",is(1)))
//	.andExpect(jsonPath("$[0].name",is("abc")))
//	.andExpect(jsonPath("$[1].id",is(2)))
//	.andExpect(jsonPath("$[1].name",is("xyz")));	
//
//}
//
//}
