package com.bby.main;

import java.io.InputStream;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        //InputStream log4j = this.getClass().getClassLoader().getResourceAsStream("log4j.properties");
        //PropertyConfigurator.configure(log4j);

		return application.sources(BigQueryApp.class);
	}
	

}