package com.bby.exception;

import java.util.Date;
import java.util.List;

public class ErrorDetails {
	private String title;
	private String status;
	  private String details;
	  private Date timestamp;
	  private String developerMessage;
	  private List<Errors> errors;
	  
	public ErrorDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ErrorDetails(String title, String status, String details, Date timestamp, String developerMessage,
			List<Errors> errors) {
		super();
		this.title = title;
		this.status = status;
		this.details = details;
		this.timestamp = timestamp;
		this.developerMessage = developerMessage;
		this.errors = errors;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getDeveloperMessage() {
		return developerMessage;
	}
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
	public List<Errors> getErrors() {
		return errors;
	}
	public void setErrors(List<Errors> errors) {
		this.errors = errors;
	}



	}
