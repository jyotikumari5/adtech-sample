package com.bby.aop;

//import com.gkatzioura.spring.aop.model.Sample;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.AfterReturning;

@Aspect
@Component
public class SampleServiceAspect {

    private static final Logger LOGGER = LogManager.getLogger(SampleServiceAspect.class);

//    @Before("execution(* com.bby.controller.*.*(..))")
//    public void before(JoinPoint joinPoint) {
//
//    	LOGGER.info(" Before hitting ");
//    	LOGGER.info(" Allowed execution for {}", joinPoint);   
//    	}
//    
//    @After("execution(* com.bby.controller.*.*(..))") 
//    public void after(JoinPoint joinPoint) {
//
//    	LOGGER.info(" after hitting ");
//    	LOGGER.info(" Allowed execution for {}", joinPoint.getSignature());   
//    	}
    
    @Pointcut("execution( * com.bby.controller.*.*(..))")
    public void Pointcut() {
    }
    @AfterReturning(value = "pointcut()", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
    	LOGGER.info("{} returned with value {}", joinPoint, result);
    }
    @After("pointcut()")
    public void after(JoinPoint joinPoint) {
    	LOGGER.info("after execution of {}", joinPoint);
    }
    @Pointcut("within(com.bby.controller.*)")//another way to define pointcut using within
    public void pointcut() {
        
    }
}
