package com.bby.configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import com.bby.bean.EmployeeClass;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ESConfiguration {

	 private static final String HOST = "localhost";
	    private static final int PORT_ONE = 9200;
	    private static final int PORT_TWO = 9201;
	    private static final String SCHEME = "http";

	    private static RestHighLevelClient restHighLevelClient;
	    private static ObjectMapper objectMapper = new ObjectMapper();

	    private static final String INDEX = "employee";
	    private static final String TYPE = "list";
	    
	    public synchronized RestHighLevelClient makeConnection() {

	        if(restHighLevelClient == null) {
	            restHighLevelClient = new RestHighLevelClient(
	                    RestClient.builder(
	                            new HttpHost(HOST, PORT_ONE, SCHEME),
	                            new HttpHost(HOST, PORT_TWO, SCHEME)));
	        }

	        return restHighLevelClient;
	    }

	    public  synchronized void closeConnection() throws IOException {
	        restHighLevelClient.close();
	        restHighLevelClient = null;
	    }
	    public  EmployeeClass insertEmployeeClass(EmployeeClass EmployeeClass){
	        EmployeeClass.setId(UUID.randomUUID().toString());
	        System.out.println(EmployeeClass);
	        Map<String, Object> dataMap = new HashMap<String, Object>();
	        dataMap.put("id", EmployeeClass.getId());
	        dataMap.put("name", EmployeeClass.getName());
	        IndexRequest indexRequest = new IndexRequest(INDEX, TYPE, EmployeeClass.getId())
	                .source(dataMap);
	        try {
	        	System.out.println(EmployeeClass.getId());
	            IndexResponse response = restHighLevelClient.index(indexRequest);
	            System.out.println(response);
//	    		LOG.log(Level.INFO, "/insertEmployeeClass - > " + response);

	        } catch(ElasticsearchException e) {
	            e.getDetailedMessage();
	        } catch (java.io.IOException ex){
	            ex.getLocalizedMessage();
	        }
	        return EmployeeClass;
	    }

	    public  EmployeeClass getEmployeeClassById(String id){
	        GetRequest getEmployeeClassRequest = new GetRequest(INDEX, TYPE, id);
	        GetResponse getResponse = null;
	        try {
	            getResponse = restHighLevelClient.get(getEmployeeClassRequest);
	            System.out.println(getResponse);
	            System.out.println(objectMapper.convertValue(getResponse.getSourceAsMap(), EmployeeClass.class));
//	    		LOG.log(Level.INFO, "/getEmployeeClassById - > " + getResponse);
	        } catch (java.io.IOException e){
	            e.getLocalizedMessage();
	        }
	        return getResponse != null ?
	                objectMapper.convertValue(getResponse.getSourceAsMap(), EmployeeClass.class) : null;
	    }

	    public  EmployeeClass updateEmployeeClassById(String id, EmployeeClass EmployeeClass){
	        UpdateRequest updateRequest = new UpdateRequest(INDEX, TYPE, id)
	                .fetchSource(true);    // Fetch Object after its update
	        try {
	            String EmployeeClassJson = objectMapper.writeValueAsString(EmployeeClass);
	            updateRequest.doc(EmployeeClassJson, XContentType.JSON);
	            UpdateResponse updateResponse = restHighLevelClient.update(updateRequest);

	            return objectMapper.convertValue(updateResponse.getGetResult().sourceAsMap(), EmployeeClass.class);
	        }catch (JsonProcessingException e){
	            e.getMessage();
	        } catch (java.io.IOException e){
	            e.getLocalizedMessage();
	        }
	        System.out.println("Unable to update EmployeeClass");
	        return null;
	    }

	    public  void deleteEmployeeClassById(String id) {
	        DeleteRequest deleteRequest = new DeleteRequest(INDEX, TYPE, id);
	        try {
	            DeleteResponse deleteResponse = restHighLevelClient.delete(deleteRequest);
	        } catch (java.io.IOException e){
	            e.getLocalizedMessage();
	        }
	    }
	    
	    

}
